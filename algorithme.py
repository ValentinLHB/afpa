nb1 = input('Saisir nombre1: ')
nb2 = input('Saisir nombre2: ')
nb3 = input('Saisir nombre3: ')

if nb1 > nb2: # cas où nb1 > nb2
    
    if nb1 > nb3: # si nb1 > nb2 et nb1 > nb3
        if nb2 > nb3: # si nb1 > nb2 > nb3
            print(
                "{} est plus grand que {} qui est plus grand que {}".format(
                    nb1,
                    nb2,
                    nb3
                )
            )
        elif nb2 < nb3: # si nb1 > nb3 > nb2
            print(
                "{} est plus grand que {} qui est plus petit que {}".format(
                    nb1,
                    nb2,
                    nb3
                )
            )
        else: # nb2 = nb3
            print(
                "{} est plus grand que {} qui est égal à {}".format(
                    nb1,
                    nb2,
                    nb3
                )
            )
    
    elif nb1 < nb3: # si nb3 > nb1 et que nb1 > nb2 alors par déduction: nb3 > nb1 > nb2
        print(
            "{} est plus grand que {} qui est plus petit que {}".format(
                nb1,
                nb2,
                nb3
            )
        )
    
    else: # sinon, par décution: nb1 = nb3
        print(
            "{} est plus grand que {} qui est plus petit que {}".format(
                nb1,
                nb2,
                nb3
            )
        )
        
elif nb1 < nb2: # cas où nb1 < nb2
    
    if nb1 < nb3: # cas où nb2 > nb1 et nb3 > nb1: quelle relation entre nb2 et nb3?
        
        if nb2 > nb3: # cas où nb2 > nb3 > nb1
            print(
                "{} est plus petit que {} qui est plus grand que {}".format(
                    nb1,
                    nb2,
                    nb3
                )
            )
        
        elif nb2 < nb3: # cas où nb1 < nb2 < nb3
            print(
                "{} est plus petit que {} qui est plus petit que {}".format(
                    nb1,
                    nb2,
                    nb3
                )
            )
        
        else: # cas où nb1 < nb2 = nb3
            print(
                "{} est plus petit que {} qui est égal à {}".format(
                    nb1,
                    nb2,
                    nb3
                )
            )

    elif nb1 > nb3: # nb1 < nb2 et nb1 > nb3: cas où nb2 > nb1 > nb3
        print(
            "{} est plus petit que {} qui est plus grand que {}".format(
                nb1,
                nb2,
                nb3
            )
        )

    else: # nb2 > nb1 = nb3
        print(
            "{} est plus petit que {} qui est plus grand que {}".format(
                nb1,
                nb2,
                nb3
            )
        )
        
elif nb1 == nb2: # cas où nb1 = nb2: il faut comparer nb1 (ou nb2) à nb3
    
    if nb1 > nb3: # cas où nb1 = nb2 > nb3

        print(
            "{} est égal à {} qui est plus grand que {}".format(
                nb1,
                nb2,
                nb3
            )
        )

    elif nb1 < nb3: # cas où nb1 = nb2 < nb3

        print(
            "{} est égal à {} qui est plus petit que {}".format(
                nb1,
                nb2,
                nb3
            )
        )

    else: # cas où nb1 = nb2 = nb3

        print(
            "{} est égal à {} qui est égal à {}".format(
                nb1,
                nb2,
                nb3
            )
        )

else:

    pass
