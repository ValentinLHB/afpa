# Créer un compte sur GitLab

https://gitlab.com/

# Créer un repository vide sur GitLab

New project -> blank project -> Donner un nom, mettre la visibilité sur public, ne pas mettre de fichier README

# Envoyer son travail en local vers le repository

1. Ouvrir git bash et aller jusqu'à son dossier de travail

2. Faire un ```git init``` pour initialiser le repo

3. Faire un ```git remote add origin git@gitlab.com:<votre_nom>/<nom_du_repo>.git```

3. Faire un ```git add .``` pour ajouter tous les fichiers dans le dossier de travail

4. Faire un ```git commit -m "description des changements"``` pour 

Pour info sur la commande ```commit```

*Le terme anglais fait référence à la commande éponyme **Commit** présente dans la plupart des systèmes de gestion de base de données et des logiciels de gestion de versions, qui ne proposent généralement pas d’interface de programmation régionalisée. Il **provient du latin committere**, de « co(m)- », signifiant « ensemble » et « mittere », signifiant « envoyer », ce terme latin a également donné en français le terme « commettre ». Il conserve en anglais plusieurs sens, celui de confier (comme on confie une mission) et d'**effectuer une action**, comme le commettre du français moderne.*

*Par extension, dans un système de gestion de versions centralisé (tel que CVS, Subversion (SVN)) ou décentralisé (tel que **Git**), la validation est **l'action d'envoyer ses modifications locales vers le référentiel central** afin, d'une part, de mettre à disposition les modifications apportées à un document et, d'autre part, d'insérer de façon cohérente ces modifications dans l'historique des modifications.*


6. Faire un ```git push -u origin master``` (si besoin, --set-upstream)